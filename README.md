# API RESTful

Esta API RESTful tem como objetivo calcular a probabilidade de ataques em localizações pré-definidas, utilizando um serviço de geolocalização do Google.

# Build / Run da aplicação

A aplicação utiliza Java 1.8 e possui como pré-requisito ter o maven instalado.

`https://maven.apache.org/download.cgi`

É necessário baixar o projeto utilizando o GIT para um diretório desejado.

`git clone https://frojas_@bitbucket.org/frojas_/gotham-backend.git`

Em seguida é necessário realiar o build do projeto através do comando a seguir:

`mvn clean install`

Para iniciar a aplicação, podemos utilizar 2 formas.

- Executando o comando abaixo dentro do diretório onde o projeto foi baixado:

- `mvn spring-boot:run`

- Empacotando e executando o jar manualmente conforme abaixo:

- `mvn package`

- `java -jar [nome-do-jar].jar`

# Endpoints

Existem 3 endpoints que utilizam o método HTTP [GET]. O localização pode ser informada através do endereço, localidade ou coordenadas. Abaixo seguem os exemplos:

`http://localhost:8080/address?q=Rua+Castro+Neves`

`http://localhost:8080/location?q=toronto`

`http://localhost:8080/coordinate?q=33.94,-118.40`

Exemplo de retorno em caso de sucesso

```
{
    "villain": {
        "name": "Joker",
        "location": {
            "lat": 45.4408474,
            "lng": 12.3155151
        }
    },
    "targets": [
        {
            "location": {
                "lat": 40.748288,
                "lng": -73.985791
            },
            "place": "Amusement Mile",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.753722,
                "lng": -73.977494
            },
            "place": "Gotham University",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.753875,
                "lng": -73.983745
            },
            "place": "Giordano Botanical Gardens",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.761687,
                "lng": -73.981873
            },
            "place": "Gotham Arms Apartment",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.759941,
                "lng": -73.975449
            },
            "place": "Old Gotham Subway",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.749428,
                "lng": -73.976931
            },
            "place": "Special Crimes Unit",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.753645,
                "lng": -73.988117
            },
            "place": "GCPD Headquarters",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.75317,
                "lng": -73.981972
            },
            "place": "Gotham City Hall",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.755469,
                "lng": -73.976731
            },
            "place": "The Clocktower",
            "probability": 95
        },
        {
            "location": {
                "lat": 40.759134,
                "lng": -73.979021
            },
            "place": "Wayne Enterprises",
            "probability": 95
        }
    ]
}
```
