package br.com.maplink.challenge.formula;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import br.com.maplink.formula.HaversineFormula;

public class HaversineFormulaTest {
	
	@Test
	public void it_should_calculate_haversine_for_a_given_position() {
		
		double result = HaversineFormula.haversine(40.746422d, -73.994753d, 40.763328d, -73.968039d);
		
		Assert.assertThat(result, Matchers.greaterThan(0d));
		
	}
	
}
