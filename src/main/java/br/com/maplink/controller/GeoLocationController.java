package br.com.maplink.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.maplink.representer.ErrorResponse;
import br.com.maplink.representer.Output;
import br.com.maplink.service.GeoLocationService;

@RestController
public class GeoLocationController {

	@Autowired
	private GeoLocationService service;

	@RequestMapping(value = "/address", method = RequestMethod.GET)
	public ResponseEntity<Output> address(
			@RequestParam(value = "q", required = true) String address)
			throws Exception {

		return new ResponseEntity<Output>(service.calculate(address),
				HttpStatus.OK);

	}

	@RequestMapping(value = "/location", method = RequestMethod.GET)
	public ResponseEntity<Output> location(
			@RequestParam(value = "q") String location) throws Exception {

		return new ResponseEntity<Output>(service.calculate(location),
				HttpStatus.OK);

	}

	@RequestMapping(value = "/coordinate", method = RequestMethod.GET)
	public ResponseEntity<Output> coordinate(
			@RequestParam(value = "q", required = true) String coordinate)
			throws Exception {

		return new ResponseEntity<Output>(service.calculate(coordinate),
				HttpStatus.OK);
	}

	@ExceptionHandler
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception exception) {

		ErrorResponse response = new ErrorResponse();
		response.setMessage(exception.getMessage());

		return new ResponseEntity<ErrorResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
