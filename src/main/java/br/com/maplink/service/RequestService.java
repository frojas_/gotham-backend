package br.com.maplink.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

@Service
public class RequestService {

	@Value(value = "${url.targets}")
	private String targetsUrl;

	@Value(value = "${url.geocode}")
	private String geocodeUrl;

	@Value(value = "${url.gotham.limit}")
	private String coordinateLimitUrl;

	@Value(value = "${api.key}")
	private String apiKey;

	public String getTargets() throws Exception {
		URL url = new URL(targetsUrl);
		return this.request(url);
	}

	public String getCoordinateLimits() throws Exception {
		URL url = new URL(coordinateLimitUrl);
		return this.request(url);
	}

	public GeocodingResult[] getGeoCodePosition(String query, LatLng southWest,
			LatLng northEast) throws Exception {

		GeoApiContext context = new GeoApiContext().setApiKey(apiKey);
		GeocodingResult[] results = GeocodingApi.geocode(context, query)
				.bounds(southWest, northEast).await();

		return results;
	}

	private String request(URL url) {

		try {

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();

			connection.setRequestMethod("GET");

			BufferedReader in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();

			return response.toString();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

}
