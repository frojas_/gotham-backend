package br.com.maplink.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maplink.formula.HaversineFormula;
import br.com.maplink.representer.CoordinateLimit;
import br.com.maplink.representer.JsonMapper;
import br.com.maplink.representer.Output;
import br.com.maplink.representer.Target;
import br.com.maplink.representer.TargetList;
import br.com.maplink.representer.Villain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

@Service
public class GeoLocationService {

	@Autowired
	private JsonMapper mapper;

	@Autowired
	private RequestService request;

	private TargetList targetList;

	private CoordinateLimit coordinateLimit;

	private static final double ATTACK_RADIUS = 2000d;

	private static final double MAX_PROBABILITY = 95.0d;

	private static final String VILLAIN_NAME = "Joker";

	@PostConstruct
	public void init() throws Exception {
		targetList = this.requestTargets();
		coordinateLimit = this.requestLimits();
	}

	private TargetList requestTargets() throws Exception {

		return mapper.read(request.getTargets(),
				new TypeReference<TargetList>() {
				});
	}

	private CoordinateLimit requestLimits() throws Exception {

		return mapper.read(request.getCoordinateLimits(),
				new TypeReference<CoordinateLimit>() {
				});
	}

	private GeocodingResult[] getPositions(String query) throws Exception {

		LatLng southWestBound = new LatLng(
				getCoordinateLimit().getSouthWest()[0], getCoordinateLimit()
						.getSouthWest()[1]);

		LatLng northEastBound = new LatLng(
				getCoordinateLimit().getNorthEast()[0], getCoordinateLimit()
						.getNorthEast()[1]);

		return request
				.getGeoCodePosition(query, southWestBound, northEastBound);

	}

	public Output calculate(String query) throws Exception {

		GeocodingResult[] results = this.getPositions(query);

		if (null == results || results.length == 0) {
			throw new Exception("Results not found in Gotham limit");
		}

		TargetList targetList = this.getTargetList();

		GeocodingResult position = results[0];
		double latPosition = position.geometry.location.lat;
		double lngPosition = position.geometry.location.lng;

		for (Target target : targetList.getTargets()) {

			double latTargetPosition = target.getLocation().getLat();
			double lngTargetPosition = target.getLocation().getLng();

			double haversine = HaversineFormula.haversine(latPosition,
					lngPosition, latTargetPosition, lngTargetPosition);

			if (haversine >= ATTACK_RADIUS) {
				target.setProbability(MAX_PROBABILITY);
			} else {
				double value = (haversine * MAX_PROBABILITY) / ATTACK_RADIUS;
				target.setProbability(value);
			}
		}

		Output output = new Output();
		output.setTargets(targetList.getTargets());
		Villain villain = new Villain(VILLAIN_NAME, latPosition, lngPosition);
		output.setVillain(villain);
		return output;
	}

	private TargetList getTargetList() throws Exception {

		if (null == targetList) {
			return this.requestTargets();
		}

		return targetList;
	}

	public void setTargetList(TargetList targets) {
		this.targetList = targets;
	}

	public CoordinateLimit getCoordinateLimit() throws Exception {

		if (null == coordinateLimit) {
			return this.requestLimits();
		}

		return coordinateLimit;
	}

	public void setCoordinateLimit(CoordinateLimit coordinateLimit) {
		this.coordinateLimit = coordinateLimit;
	}

}
