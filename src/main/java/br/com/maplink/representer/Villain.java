package br.com.maplink.representer;

public class Villain {

	private String name;

	private Location location;

	public Villain(String name, double latPosition, double lngPosition) {
		this.name = name;
		Location location = new Location();
		location.setLat(latPosition);
		location.setLng(lngPosition);
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

}
