package br.com.maplink.representer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CoordinateLimit {

	@JsonProperty("bottom_left")
	private double[] southWest;

	@JsonProperty("top_right")
	private double[] northEast;

	public double[] getSouthWest() {
		return southWest;
	}

	public void setSouthWest(double[] southWest) {
		this.southWest = southWest;
	}

	public double[] getNorthEast() {
		return northEast;
	}

	public void setNorthEast(double[] northEast) {
		this.northEast = northEast;
	}

}
