package br.com.maplink.representer;

import java.util.List;

public class TargetList {

	private List<Target> targets;

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List<Target> targets) {
		this.targets = targets;
	}
	
	
	
}
