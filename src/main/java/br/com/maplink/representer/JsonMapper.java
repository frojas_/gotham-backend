package br.com.maplink.representer;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JsonMapper {

	public <T> T read(String json, final TypeReference<T> reference) {

		ObjectMapper mapper = new ObjectMapper();

		try {
			return mapper.readValue(json, reference);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
