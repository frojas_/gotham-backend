package br.com.maplink.representer;

import java.util.List;

public class Output {

	private Villain villain;

	private List<Target> targets;

	public Villain getVillain() {
		return villain;
	}

	public void setVillain(Villain villain) {
		this.villain = villain;
	}

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List<Target> targets) {
		this.targets = targets;
	}

}
